<?php
    require_once("animal.php");
    require_once("frog.php");
    require_once("ape.php");
    $sheep = new Animal("shaun");
    echo "Name: ".$sheep->name. "<br>";
    echo "Legs: ".$sheep->legs. "<br>";
    echo "Cold Blooded: ".$sheep->cold_blooded."<br>"."<br>";

    $katak = new frog("Buduk");
    echo "Name: ".$katak->name. "<br>";
    echo "Legs: ".$katak->legs. "<br>";
    echo "Cold Blooded: ".$katak->cold_blooded."<br>";
    echo $katak->jump(). "<br><br>";

    $Monyet = new ape("Sungokong");
    echo "Name: ".$Monyet->name. "<br>";
    echo "Legs: ".$Monyet->legs. "<br>";
    echo "Cold Blooded: ".$Monyet->cold_blooded."<br>";
    echo $Monyet->yell();
?>